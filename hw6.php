<?php

require_once('common.php');

class Hw6Tests extends HwTests {

    function testsNotReady() {
        $this->fail('Tests for hw6 are not ready yet');
    }

}

(new Hw6Tests())->run(new PointsReporter());
