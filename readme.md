# ICD0007 Project Tests

### Running Coding Standard Checks

`php vendor/phpcs.phar -n -s --standard=config/phpcs.xml <project directory>`

### Running Tests for Part 3
`php hw3.php`

### Running Tests for Part 4
`php hw4.php`

### Running Tests for Part 5
`php hw5.php`

### Running Tests for Part 6
`php hw6.php`
